import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class Json_con_cadenas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Primera invocación en capa WS");
		
		//Variables para invocar el servicio
		String url="https://oim.mapfre.com.pe/oim_new_login/api/login";
		String cuerpo_ws="";
		String autotencicacion="";
		
		//Parametrización del body
		cuerpo_ws="client_Id=099153c2625149bc8ecb3e85e03f0022&grant_type=password&password="+"Multiplica123"+"&subType=&type=1&userName=MULTIPLICA";
		
		   //Invocación de librería RestAssurd
		RestAssured.baseURI=url; //Sintaxis para llamar a la URL del servicio
		
		RequestSpecification request=RestAssured.given();//Captura del Servicio con la URI o URL <> F12 del Chrome
		
		request.body(cuerpo_ws); //Declaración del body del WS
		
		Response respuesta=request.post();//Llamada por el método, relacionado al body del WS
		
		System.out.println("La respuesta del servicio en código es: "+respuesta.getStatusCode()); //Obtener el cod de Respta del WS		
		System.out.println("El tiempo de respuesta del WS es: "+ respuesta.getTime()+" en ms");
		
		//Obteniendo variables del Json independientes
		String token="";
		token=respuesta.jsonPath().get("access_token"); //Capturando el resultado de un elemento del json
		
		int tiempo_de_expiracion;
		tiempo_de_expiracion=respuesta.jsonPath().get("expires_in");
		System.out.println(tiempo_de_expiracion);
		
		String tipo_token="";
		tipo_token=respuesta.jsonPath().get("token_type");
		System.out.println(tipo_token);
		
		//Obteniendo variables de Json con trama completa
		String trama_token_loggin="";
		trama_token_loggin=respuesta.getBody().asString();
		System.out.println(trama_token_loggin);
		
		//Validar que el tipo de token de loggin OIM sea Bearer
		
		/*
		 Si el respuesta del servicio es igual a Bearer
		 consultando el path de token_type
		   Se responde validación correcta
		 Si no es asi
		   Mensaje de error, mostrando el resultado real obtenido  		   
		*/
		//Solución 1
		if(tipo_token.equals("bearer"))
		{
			System.out.println("Validación correcta");
		}
		else
		{
			System.out.println("Falla, el tipo de token es "
			+ "incorrecto. El token obtenido es:"+tipo_token);
		}
		
		//Solución 2
		String aux=trama_token_loggin;
		System.out.println("Sol 2");
		
		//Obteniendo ubicación del inicio de token_type
		String string_de_inicio="token_type\":\"";
		String string_de_fin="\",\"expires_in";
		
		int pos_inicio_token_type;
		pos_inicio_token_type=aux.indexOf(string_de_inicio);
		System.out.println(pos_inicio_token_type);
		pos_inicio_token_type=pos_inicio_token_type+string_de_inicio.length();
		
		//Obteniendo ubicación de fin de token_type
		int pos_fin_token_type;
		pos_fin_token_type=aux.indexOf(string_de_fin);
		System.out.println(pos_fin_token_type);
	
		
		String valor_tipo_aux=aux.substring(pos_inicio_token_type,
				pos_fin_token_type);
				
		if(valor_tipo_aux.equals("bearer"))
		{
			System.out.println("Validación correcta");
		}
		else
		{
			System.out.println("Falla, el tipo de token es "
			+ "incorrecto. El token obtenido es:"+aux);
		}
		
		
		System.out.println("Fin de invocación del servicio");
	}

}
