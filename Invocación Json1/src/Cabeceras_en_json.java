import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class Caberas_ws {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Primera invocación en capa WS");
		
		//Variables para invocar el servicio
		String url="https://oim.mapfre.com.pe/oim_new_login/api/login";
		String cuerpo_ws="";
		String autotencicacion="";
		
		cuerpo_ws="client_Id=099153c2625149bc8ecb3e85e03f0022&grant_type=password&password="+"Multiplica123"+"&subType=&type=1&userName=MULTIPLICA";
		
		RestAssured.baseURI=url; //Sintaxis para llamar a la URL del servicio
		
		RequestSpecification request=RestAssured.given();//Captura del Servicio con la URI o URL <> F12 del Chrome		
		request.body(cuerpo_ws); //Declaración del body del WS
		
		Response respuesta=request.post();//Llamada por el método, relacionado al body del WS
		
		//Grabar en varibles los json del ws
		String token_1="";
		token_1=respuesta.jsonPath().getString("access_token");
		
		System.out.println("La respuesta del servicio en código es: "+respuesta.getStatusCode()); //Obtener el cod de Respta del WS		
		System.out.println("El tiempo de respuesta del WS es: "+ respuesta.getTime()+" en ms");
		
		System.out.println("Invocación a token1: "+token_1);
		
		//Invocación al 2do servicio de claims para token2
		String url_servicio_grp_claims_token2=
		"https://oim.mapfre.com.pe/oim_new_login/api/claims/GenerateClaimsByGroupType";
		String cuerpo_servicio_grp_claims_token2="GroupTypeId=1";
		
		RestAssured.baseURI=url_servicio_grp_claims_token2;
		RequestSpecification llamada_token2=RestAssured.given();
		
		//Agregar parámetros de Header
		Header cabecera=new Header("Authorization", "Bearer "+token_1);
		llamada_token2.header(cabecera);
		
		llamada_token2.body(cuerpo_servicio_grp_claims_token2);
		llamada_token2.contentType("application/x-www-form-urlencoded");
		
		Response respuesta_token2=llamada_token2.post();
		
		String token2=respuesta_token2.jsonPath().getString("access_token");
		
		System.out.println("Invocación a token2: "+token2);
		
		System.out.println("Fin de invocación del servicio");

	}

}
