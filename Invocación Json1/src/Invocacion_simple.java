import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class Invocacion_simple {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Primera invocaci�n en capa WS");
		
		//Variables para invocar el servicio
		String url="https://oim.mapfre.com.pe/oim_new_login/api/login";
		String cuerpo_ws="";
		String autotencicacion="";
		
		//Parametrizaci�n del body
		cuerpo_ws="client_Id=099153c2625149bc8ecb3e85e03f0022&grant_type=password&password="+"Multiplica123"+"&subType=&type=1&userName=MULTIPLICA";
		
		   //Invocaci�n de librer�a RestAssurd
		RestAssured.baseURI=url; //Sintaxis para llamar a la URL del servicio
		
		RequestSpecification request=RestAssured.given();//Captura del Servicio con la URI o URL <> F12 del Chrome
		
		request.body(cuerpo_ws); //Declaraci�n del body del WS
		
		Response respuesta=request.post();//Llamada por el m�todo, relacionado al body del WS
		
		System.out.println("La respuesta del servicio en c�digo es: "+respuesta.getStatusCode()); //Obtener el cod de Respta del WS		
		System.out.println("El tiempo de respuesta del WS es: "+ respuesta.getTime()+" en ms");
		
		//Obteniendo variables del Json
		String token="";
		token=respuesta.jsonPath().get("access_token"); //Capturando el resultado de un elemento del json
		token=respuesta.getBody().asString(); //Obteniendo todo el cuerpo del WS
		System.out.println(token);
		
		System.out.println("Fin de invocaci�n del servicio");
	}

}
