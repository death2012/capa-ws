
public class Mini_ejercicio_cadenas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String cadena1="Exámen es el día: Jueves";
		
		//Validar si el exámen dice Jueves
		//y mostrar en pantalla el resultado
		String valor_del_dia="";
		valor_del_dia=cadena1.substring(cadena1.indexOf(":")+2,
				cadena1.length());
		System.out.println(valor_del_dia);
		if(valor_del_dia.equals("Jueves"))
		{
			System.out.println("Es correcto");
		}
		else
		{
			System.out.println("Es incorrecto");
		}
		
		//Ejemplo 2
		String cadena2="Tipo de Token: Spect";
			
		if(cadena2.substring(cadena2.indexOf(": ")+2, cadena2.length()).equals("Spect"))
		{
			System.out.println("Es correcto");
		}
		else
		{
			System.out.println("Es incorrecto");
		}
		
	}

}
